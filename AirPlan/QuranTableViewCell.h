//
//  QuranTableViewCell.h
//  AirPlan
//
//  Created by MAcBookPro on 1/21/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuranTableViewController.h"

@interface QuranTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cellName;
- (IBAction)playPause:(id)sender;
@property NSMutableString *cellUrl;
@property QuranTableViewController *myController;
@end
