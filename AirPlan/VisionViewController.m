//
//  VisionViewController.m
//  AirPlan
//
//  Created by MAcBookPro on 1/22/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import "VisionViewController.h"

@interface VisionViewController ()

@end

@implementation VisionViewController
@synthesize  url ;
@synthesize  parser ;
@synthesize feeds ;
@synthesize item ;
@synthesize element;
@synthesize visionHTMLString;
@synthesize visionWebView;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
//    NSString *html = @"<h2>BEING A BENCHMARK</h2><br><span>AlphaStar is a Benchmark in Aircraft Management and Private Aviation Business</span><br>";
//    
//    [self.visionWebView loadHTMLString:html baseURL:nil];
//    [self.visionWebView setDelegate:self];
    //NSLog(@"visionHTMLString %@",visionHTMLString);
    
    url = [[NSURL alloc]initWithString:@"http://mohamedsoudi-001-site2.smarterasp.net/service/update_section?section_id=1"];
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        // Perform long running process
        feeds = [[NSMutableArray alloc] init];
        parser = [[NSXMLParser alloc]initWithContentsOfURL:url];
        [parser setDelegate:self];
        [parser parse];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.visionWebView loadHTMLString:visionHTMLString baseURL:nil];
            [self.visionWebView setDelegate:self];
             NSLog(@"visionHTMLString %@",visionHTMLString);
        });
    });
    // Do any additional setup after loading the view.
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    element = elementName;
    
    if ([element isEqualToString:@"vision"]) {
        item    = [[NSMutableDictionary alloc] init];
        visionHTMLString   = [[NSMutableString alloc] init];
    }
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([element isEqualToString:@"vision"]) {
        [visionHTMLString appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"vision"]) {
        
        [item setObject:visionHTMLString forKey:@"vision"];
        //[feeds addObject:[item copy]];
        
    }
    
    
    
}

-(void)parserDidEndDocument:(NSXMLParser *)parser {
//    [self.visionWebView loadHTMLString:visionHTMLString baseURL:nil];
//    [self.visionWebView setDelegate:self];
//    NSLog(@"visionHTMLString %@",visionHTMLString);

    
}



-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
