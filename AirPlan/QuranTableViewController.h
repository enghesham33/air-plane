//
//  QuranTableViewController.h
//  AirPlan
//
//  Created by MAcBookPro on 1/19/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuranTableViewController : UITableViewController <NSXMLParserDelegate>

@property NSURL *url;
@property NSXMLParser *parser;
@property NSMutableArray *feeds;
@property NSMutableDictionary *item;
@property NSMutableString *soraName;
@property NSMutableString *soraPath;
@property NSMutableString *readerName;
@property NSString *element;

@end
