//
//  DirectorMessageViewController.m
//  AirPlan
//
//  Created by MAcBookPro on 1/22/15.
//  Copyright (c) 2015 MAcBookPro. All rights reserved.
//

#import "DirectorMessageViewController.h"

@interface DirectorMessageViewController ()

@end

@implementation DirectorMessageViewController

@synthesize  url ;
@synthesize  parser ;
@synthesize feeds ;
@synthesize item ;
@synthesize element;
@synthesize directorMessageHTMLString;
@synthesize directorMessageWebView;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    self.directorMessageWebView.backgroundColor =[UIColor clearColor];
    
    
    url = [[NSURL alloc]initWithString:@"http://mohamedsoudi-001-site2.smarterasp.net/service/update_section?section_id=1"];
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        // Perform long running process
        feeds = [[NSMutableArray alloc] init];
        parser = [[NSXMLParser alloc]initWithContentsOfURL:url];
        [parser setDelegate:self];
        [parser parse];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.directorMessageWebView loadHTMLString:directorMessageHTMLString baseURL:nil];
            [self.directorMessageWebView setDelegate:self];
            NSLog(@"directorMessageHTMLString %@",directorMessageHTMLString);
        });
    });
    // Do any additional setup after loading the view.
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    element = elementName;
    
    if ([element isEqualToString:@"director_message"]) {
        item    = [[NSMutableDictionary alloc] init];
        directorMessageHTMLString   = [[NSMutableString alloc] init];
    }
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([element isEqualToString:@"director_message"]) {
        [directorMessageHTMLString appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"director_message"]) {
        
        [item setObject:directorMessageHTMLString forKey:@"director_message"];
        //[feeds addObject:[item copy]];
        
    }
    
}

-(void)parserDidEndDocument:(NSXMLParser *)parser {
    //    [self.visionWebView loadHTMLString:visionHTMLString baseURL:nil];
    //    [self.visionWebView setDelegate:self];
    //    NSLog(@"visionHTMLString %@",visionHTMLString);
    
    
}



-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
